function CFD_PROJECT2(n)
% define global variables for inter-function use **********
global x;
global y;
global h;
global n_g;
global NN;
global NE;
global JacobianMatrix;
global InverseJacobianMatrix;
global J;
% *********************************************************
n_g = n;

NN = n*n;
min_coordinate = 0.; % x and y coordinates are from 0 to 1 *
max_coordinate = 1.; % *************************************

% grid resolution *****************************************
sym h;
sym alpha;
sym J;
h = (max_coordinate - min_coordinate)/(n - 1.); % x and y are equally spaced
%**********************************************************

alpha = 1/10;
delta_T = 0.5*h*h/(4.*alpha); % ADJUST TIME STEP!
T = 0;
% Jacobian for coordinate transformation ******************
JacobianMatrix = [vpa(h/2.), 0; 0, vpa(h/2.)];
J = det(JacobianMatrix);
InverseJacobianMatrix = vpa(1/J) * [vpa(h/2.), 0; 0, vpa(h/2.)];
%**********************************************************

% coordinate definitions **********************************
x = min_coordinate:h:max_coordinate;
y = min_coordinate:h:max_coordinate;
% *********************************************************

% number of elements **************************************
NE = (n - 1) * (n - 1);
% *********************************************************

% define u(x, y) ******************************************
u = zeros(n, n);
u(:, n) = x - x.*x; % impose boundary conditions*
b_v = zeros(NN, 1);
for i = 1:n
    for j = 1:n
    b_v(j + (i - 1)*n, 1) = u(j, i);
    end
end
% *********************************************************

K_local = LocalStiffnessMatrix();
LtoG = zeros(NE, 4);
d = 1;
for i=1:NE
    if(mod(d, n) == 0)
        d = d+1;
    end
    LtoG(i, :) = [d, d + 1, d + n + 1, d + n];
    d = d + 1;
end

K_global = sparse(NN, NN);
for i=1:NE
    for m=1:4
        for s=1:4
            K_global(LtoG(i, m), LtoG(i, s)) =  K_global(LtoG(i, m), LtoG(i, s)) + K_local(m, s);
        end
    end
end

M_local = MassMatrix();
M_global = sparse(NN, NN);
for i=1:NE
    for m=1:4
        for s=1:4
            M_global(LtoG(i, m), LtoG(i, s)) =  M_global(LtoG(i, m), LtoG(i, s)) + M_local(m, s);
        end
    end
end

for i = 1:n
    K_global(i, :) = 0;
    K_global(i, i) = 1;
    M_global(i, :) = 0;
    M_global(i, i) = 1;
    K_global(NN - (i - 1), :) = 0;
    K_global(NN - (i - 1), NN - (i - 1)) = 1;
    M_global(NN - (i - 1), :) = 0;
    M_global(NN - (i - 1), NN - (i - 1)) = 1;
end


for i = 1:NN
    if(mod(i, n) == 0 || mod(i,n) == 1)
        K_global(i, :) = 0;
        K_global(i, i) = 1;
        M_global(i, :) = 0;
        M_global(i, i) = 1;
    end
end

%EXPLICIT
u_v = zeros(NN, 1);
u_v_new = zeros(NN, 1);
T = 0;
A = M_global;
while(T < 1)
    B = alpha * delta_T * b_v +  (M_global - alpha * delta_T * K_global) * u_v;
    u_v_new = gmres(A,B,[],[],n);
    u_v = u_v_new;
    T = T + delta_T;
end

for i = 1:n
    for j = 1:n
    u(j,i) = u_v(j + (i-1)*n);
    end
end

figure();
contourf(x, y, u, 20);
xlabel('y');
ylabel('x');
title('Explicit solution at T = 1 s');

%CRANK-NICHOLSON
u_v = zeros(NN, 1);
u_v_new = zeros(NN, 1);
theta = 0.5;
T = 0;
A = (M_global + theta * alpha * delta_T * K_global);
while(T < 1)
    B = alpha * delta_T * b_v +  (M_global - (1 - theta) * alpha * delta_T * K_global) * u_v;
    u_v_new = gmres(A,B,[],[],n);
    u_v = u_v_new;
    T = T + delta_T;
end

figure();

for i = 1:n
    for j = 1:n
    u(j,i) = u_v(j + (i-1)*n);
    end
end
contourf(x, y, u, 20);
xlabel('y');
ylabel('x');
title('Crank-Nicholson solution at T = 1 s');

%IMPLICIT
u_v = zeros(NN, 1);
u_v_new = zeros(NN, 1);
T = 0;
A = M_global + alpha * delta_T * K_global;
while(T < 1)
    B = alpha * delta_T * b_v +  (M_global) * u_v;
    u_v_new = gmres(A,B,[],[],n);
    u_v = u_v_new;
    T = T + delta_T;
end

figure();

for i = 1:n
    for j = 1:n
    u(j,i) = u_v(j + (i-1)*n);
    end
end
contourf(x, y, u, 20);
xlabel('y');
ylabel('x');
title('Implicit solution at T = 1 s');

u_v = zeros(NN, 1);
u_v = gmres(K_global,b_v,[],[],n);

figure();

for i = 1:n
   for j = 1:n
    u(j,i) = u_v(j + (i-1)*n);
    end
end
disp(K_global);
disp(u);
contourf(x, y, u, 20);
xlabel('y');
ylabel('x');
title('Steady state solution');


figure();
u_a = AnalyticSolution();
contourf(x, y, u_a, 20);
xlabel('y');
ylabel('x');
title('Analytical solution');
disp(u_a);
disp('ERROR:');
disp(norm(u - u_a)/n);
end

function result = AnalyticSolution()
    global n_g;
    global x;
    global y;
    u_a = zeros(n_g, n_g);
    for i=1:n_g
        for j=1:n_g
            for k=1:100
                u_a(i,j) = u_a(i,j) + 2*(2 - 2*(-1)^k)*csch(pi*k)*sin(pi*x(i)*k)*...
                sinh(pi*y(j)*k) / ...
                    (pi^3*k^3);
            end
        end
    end
    result = u_a;
end

function result = MassMatrix()
    global J;
    M = zeros(4, 4);
    for i=1:4
       for j=1:4
           % Gauss quadrature for 2D
           w = [5./9, 8./9, 5./9];
           eta = [-sqrt(3./5), 0., sqrt(3./5)];
           for n = 1:3 
               for m=1:3
                 M(i, j) = M(i, j) + ...
                     w(n)*w(m)* ... 
                     (phi(i, eta(n), eta(m))*phi(j, eta(n), eta(m))) * J;%
               end
           end
       end 
    end
    result = M;
end

function result = LocalStiffnessMatrix()
    global J;
    K = zeros(4, 4);
    for i=1:4
       for j=1:4
           % Gauss quadrature for 2D
           w = [vpa(5./9), vpa(8./9), vpa(5./9)];
           eta = [vpa(-sqrt(3./5)), 0., vpa(sqrt(3./5))];
           for n = 1:3 
               for m=1:3
                 K(i, j) = K(i, j) + ...
                     w(n)*w(m)* ... 
                     (dphidx(i, eta(n), eta(m))*dphidx(j, eta(n), eta(m)) + ...
                     dphidy(i, eta(n), eta(m))*dphidy(j, eta(n), eta(m))) * J;%
               end
           end
       end 
    end
    result = K;
end

% **********************************************
% phi functions with in terms of ksi and eta ***
function result = phi(i, ksi, eta)
    if i==1
        result = phi_1(ksi, eta);
    end
    if i==2
        result = phi_2(ksi, eta);
    end
    if i==3
        result = phi_3(ksi, eta);
    end
    if i==4
        result = phi_4(ksi, eta);
    end
end
% **********************************************

% **********************************************
% derivative of phi function with respect to x
function result = dphidx(i, ksi, eta)
    global InverseJacobianMatrix;
    A = InverseJacobianMatrix*[dphidksi(i, eta); dphideta(i, ksi)];
    result = A(1);
end
function result = dphidy(i, ksi, eta)
    global InverseJacobianMatrix;
    A = InverseJacobianMatrix*[dphidksi(i, eta); dphideta(i, ksi)];
    result = A(2);
end
% derivative of phi function with respect to ksi
function result = dphidksi(i, eta)
    if i==1
        result = -0.25 * (1 - eta);
    end
    if i==2
        result = 0.25 * (1 - eta);
    end
    if i==3
        result = 0.25 * (1 + eta);
    end
    if i==4
        result = -0.25 * (1 + eta);
    end
end
% derivative of phi function with respect to eta
function result = dphideta(i, ksi)
if i==1
    result = -0.25 * (1 - ksi);
end
if i==2
    result = -0.25 * (1 + ksi);
end
if i==3
    result = 0.25 * (1 + ksi);
end
if i==4
    result = 0.25 * (1 - ksi);
end
end
% **********************************************

% phi functions in terms of ksi and eta **************
function result = phi_1(ksi, eta)
result = 0.25 * (1-ksi)*(1-eta);
end
function result = phi_2(ksi, eta)
result = 0.25 * (1+ksi)*(1-eta);
end
function result = phi_3(ksi, eta)
result = 0.25 * (1+ksi)*(1+eta);
end
function result = phi_4(ksi, eta)
result = 0.25 * (1-ksi)*(1+eta);
end
% ****************************************************



