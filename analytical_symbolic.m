syms u(x,y,t)
eqn = diff(u,t) == 0.1*(diff(u, x, 2) + diff(u, y, 2));
cond = [u(x, 0, t) == 0, u(x, 1, t) == x - x^2, ...
    u(0, y, t) == 0, u(1, y, t) == 0];
ySol(x, y, t) = dsolve(eqn,cond);